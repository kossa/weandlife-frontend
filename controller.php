<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class WeandlifeController extends JControllerLegacy
{
	public function display($cachable = false, $urlparams = false){

		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		if ($user->id == 0) {
			$app->redirect("index.php?option=com_users&view=login", "Veuillez vous connecter");
		}
		parent::display();
	}
}