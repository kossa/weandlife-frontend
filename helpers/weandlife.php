<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */

defined('_JEXEC') or die;

abstract class WeandlifeHelper
{
	/**
	 Before to save it in the data base
	*/
	public static function cryptdate($x) {
		return date_parse($x)['year'].'-'.date_parse($x)['month'].'-'.date_parse($x)['day'];
	}

	/**
	 After Loading from the data base
	*/
	public static function decryptdate($x='') {
		return date_parse($x)['day'].'-'.date_parse($x)['month'].'-'.date_parse($x)['year'];
	}

	/**
	 GetCompleteProfileObject
	*/
	public static function GetCompleteProfileObject($jinput) {
		$object = new stdClass();
		$object->id_user = JFactory::getUser()->id;
		$object->nom = $jinput->get('nom','','STRING');
		$object->mail = $jinput->get('mail','','STRING');
		$x = $jinput->get('date_naissance','','STRING');
		$object->date_naissance = WeandlifeHelper::cryptdate($x);
		$object->vaccins = WeandlifeHelper::VaccinsBluid($object->date_naissance);
		$object->gender = $jinput->get('gender','');
		$object->nb_enfant = $jinput->get('nb_enfant','UINT');
		$object->fonction = $jinput->get('fonction','','STRING');
		$object->adresse = $jinput->get('adresse','','STRING');
		$object->ville = $jinput->get('ville','','STRING');
		$object->wilaya = $jinput->get('wilaya','','STRING');
		$object->mobile1 = $jinput->get('mobile1','','STRING');
		$object->mobile2 = $jinput->get('mobile2','','STRING');
		$object->tel = $jinput->get('tel','','STRING');

		return $object;
	}

	/**
	 get Contact String
	*/
	public static function getContactString($jinput) {
		return '{
						"access":"'.$jinput->get('access','public').'",
						"person1":{
							"fullname":"'.$jinput->get('fullname1','','STRING').'",
							"mobile":"'.$jinput->get('mobile1','','STRING').'",
							"fixe":"'.$jinput->get('fixe1','','STRING').'",
							"mail":"'.$jinput->get('mail1','','STRING').'"
						},
						"person2":{
							"fullname":"'.$jinput->get('fullname2','','STRING').'",
							"mobile":"'.$jinput->get('mobile2','','STRING').'",
							"fixe":"'.$jinput->get('fixe2','','STRING').'",
							"mail":"'.$jinput->get('mail2','','STRING').'"
						},
						"person3":{
							"fullname":"'.$jinput->get('fullname3','','STRING').'",
							"mobile":"'.$jinput->get('mobile3','','STRING').'",
							"fixe":"'.$jinput->get('fixe3','','STRING').'",
							"mail":"'.$jinput->get('mail3','','STRING').'"
						}
					}';
	}

	/**
	 getFicheParentString
	*/
	public static function getFicheParentString($jinput) {
		return '{
						"access":"'.$jinput->get('access','public').'",
						"dad":{
							"fullname":"'.$jinput->get('dad-fullname','','STRING').'",
							"birthday":"'.$jinput->get('dad-birthday','','STRING').'",
							"profession":"'.$jinput->get('dad-profession','','STRING').'",
							"num_security":"'.$jinput->get('dad-num_security','','STRING').'",
							"groupe_sanguin":"'.$jinput->get('dad-groupe_sanguin','','STRING').'",
							"maladie_genetique":"'.$jinput->get('dad-maladie_genetique','','STRING').'"
						},
						"mom":{
							"fullname":"'.$jinput->get('mom-fullname','','STRING').'",
							"birthday":"'.$jinput->get('mom-birthday','','STRING').'",
							"profession":"'.$jinput->get('mom-profession','','STRING').'",
							"num_security":"'.$jinput->get('mom-num_security','','STRING').'",
							"groupe_sanguin":"'.$jinput->get('mom-groupe_sanguin','','STRING').'",
							"maladie_genetique":"'.$jinput->get('mom-maladie_genetique','','STRING').'"
						}

					}';
	}

	/**
	 getMyDateValueString
	*/
	public static function getMyDateValueString($jinput, $model, $datevalue) {
		$str = '{"access":"'.$jinput->get('access','public').'","'.$model.'":[';
			for ($i=0; $i < sizeof($datevalue); $i++) { 
				foreach ($datevalue[$i] as $key => $value) {
					if ($value != "") {
						$str.= '{"'.$key.'":"'.$value.'"},';	
					}
				}
			}
			// Delete last ","
			$str = (substr($str, -1) == ",") ? substr($str, 0, -1) : $str ;
			$str.= ']}';
			return $str;
	}

	/**
		Get Message for home page notifcation after saving
	*/
	public static function getMsg($model='') {
		$titles = array('tension' => "Tension", 'poids' => "Poids", 'taille' => "Taille", 'consultations' => "Mes Consultations", 'maladies' => "Pathologies (maladies)", 'operations' => "Interventions (opérations)", 'traitements' => "Traitements médicaments ", 'maladies_chroniques' => "Maladies génétiques", 'grossesse' => "Grossesse Maternité", 'allergies' => "Troubles Allergies", 'accidents' => "Accidents travail", 'cure' => "Cure, repos en forme", 'vie_social' => "Communication et vie sociale", 'consommations_interdites' => "Consommations interdites", 'assurence' => "Assurance");
		return $titles[$model];
	}

	/**
		Get String of Don organe
	*/
	public static function getMyDonOrganeString($jinput) {
			$obj = new stdClass();
			$obj->sang = $jinput->get('sang','');
			$obj->plaquettes = $jinput->get('plaquettes','');
			$obj->foie = $jinput->get('foie','');
			$obj->rein = $jinput->get('rein','');
			$obj->coeur = $jinput->get('coeur','');
			$obj->lobe = $jinput->get('lobe','');
			$obj->poumon = $jinput->get('poumon','');
			$obj->pancreas = $jinput->get('pancreas','');
			$obj->coeur_poumon = $jinput->get('coeur_poumon','');
			$obj->intestin = $jinput->get('intestin','');
			$obj->cornee = $jinput->get('cornee','');
			$obj->os = $jinput->get('os','');
			$obj->cartilage = $jinput->get('cartilage','');
			$obj->fragments = $jinput->get('fragments','');
			$obj->moelle = $jinput->get('moelle','');
			$obj->tissus = $jinput->get('tissus','');

			$str = '{"access":"'.$jinput->get('access','public').'",';
			foreach ($obj as $key => $value) {
				$str .= '"'.$key.'":"'.$value.'",';
			}
			$str = (substr($str, -1) == ",") ? substr($str, 0, -1) : $str ;
			$str.= '}';
			
			return $str;
	}

	/**
		Get String of RDV
	*/
	public static function getMyRdvString($jinput) {
		$str = '{"access":"'.$jinput->get('access','public').'","rdv":[';
		$rdv_date = array_combine($jinput->get('rdv','','ARRAY'), $jinput->get('date','','ARRAY'));

		foreach ($rdv_date as $rdv => $date) {
			if ($rdv != '' && $date!= '') {
				$str .= '{"'.$rdv.'":"'.self::cryptdate($date).'"},';
			}
		}
		$str = (substr($str, -1) == ",") ? substr($str, 0, -1) : $str ;
		$str.= ']}';
		return $str;
	}

	/**
		Get String of Vaccins
	*/
	public static function getMyvaccinsString($jinput) {
		$str = '{"access":"'.$jinput->get('access','public').'","vaccins":[';
		$vaccins_date = array_combine($jinput->get('vaccins','','ARRAY'), $jinput->get('date','','ARRAY'));

		foreach ($vaccins_date as $vaccins => $date) {
			if ($vaccins != '' && $date!= '') {
				$str .= '{"'.$vaccins.'":"'.self::cryptdate($date).'"},';
			}
		}
		$str = (substr($str, -1) == ",") ? substr($str, 0, -1) : $str ;
		$str.= ']}';
		return $str;
	}

	/**
	 Bluid Vaccins List
	*/
	public static function VaccinsBluid($date='')
	{
		$str = '{"access":"private","vaccins":[';
		$str .= '{"Naissance : B.C.G Polio oral HBV (1)":"'.date('Y-m-d',strtotime($date) + (3600*24*1)).'"},';
		$str .= '{"1er Mois : Hépatite virale HBV (2)":"'.date ( 'Y-m-j' , strtotime ( '1 month' , strtotime ($date) ) ).'"},';
		$str .= '{"3ème Mois : D.T. Coq Hib Polio oral":"'.date ( 'Y-m-j' , strtotime ( '3 month' , strtotime ($date) ) ).'"},';
		$str .= '{"4ème Mois : D.T. Coq Hib Polio oral HBV":"'.date ( 'Y-m-j' , strtotime ( '4 month' , strtotime ($date) ) ).'"},';
		$str .= '{"5ème Mois : D.T. Coq Hib Polio oratl HBV (3)":"'.date ( 'Y-m-j' , strtotime ( '5 month' , strtotime ($date) ) ).'"},';
		$str .= '{"9ème Mois : Antirougeoleux (rougeole)":"'.date ( 'Y-m-j' , strtotime ( '9 month' , strtotime ($date) ) ).'"},';
		$str .= '{"18ème Mois : D.T. Coq Hib Polio oral":"'.date ( 'Y-m-j' , strtotime ( '18 month' , strtotime ($date) ) ).'"},';
		$str .= '{"6 ans : D.T. Enfant Polio oral Antirougeoleux":"'.date ( 'Y-m-j' , strtotime ( '6 year' , strtotime ($date) ) ).'"}';
		$str.= ']}';

		return $str;
	}

	/**
	 Get Age in public Profile
	*/
	public static function getAge($naissance){
		return ($naissance == '0-0-0') ? 0 : date('Y')-explode('-',$naissance)[2];
	}


	/**
	 Get latest Values in public Profile
	*/
	public static function getLatestValue($str='', $type)
	{
		$values = json_decode($str)->$type;

		foreach ($values[0] as $key => $value) {
			return $value;
		}
	}

	/**
	 Get List Donneur in public Profile
	*/
	public static function getDonneurs($donorgane=''){
		$Dons = array("sang" => "Sang",
			"plaquettes" => "Plaquettes",
			"foie" => "Foie",
			"rein" => "Rein",
			"coeur" => "Cœur",
			"lobe" => "Lobe hépatique , pulmonaire",
			"poumon" => "Poumon",
			"pancreas" => "Pancrèas",
			"coeur_poumon" => "Coeur-poumon",
			"intestin" => "Intestin",
			"cornee" => "Cornée",
			"os" => "Os",
			"cartilage" => "Cartilage",
			"fragments" => "Fragments osseux",
			"moelle" => "Mœlle osseuse",
			"tissus"=>"Tissus épidermique"
			);
		$str = '';
		if ($donorgane != '') {
			foreach (json_decode($donorgane) as $key => $value) {
				if($value == 'checked'){
					$str .= ' '.$Dons[$key] . ' /';
				} 
			}
		}
		return substr($str, 1, -1);
	}
}

