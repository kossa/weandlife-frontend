<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
$data = $this->data;

?>

<section class="p20 fiche-parent">
	<form action="index.php?option=com_weandlife&task=person.setmyficheparent&tmpl=component" method="POST" class="mon-modal mon-carnet" target="_top">
		<header class="top">
			<h2 class="title left">Fiche Parents</h2>
			<div class="access">
				<ul class="inline">
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios1" value="private" <?php if(isset($data->access) && $data->access == "private") echo "checked";?>>
						  Accés privé
						</label>
					</li>
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios2" value="public" <?php if(isset($data->access) && $data->access == "public") echo "checked";?>>
						  Accés public
						</label>
					</li>
				</ul>
			</div>
		</header>
		<div class="the-form">
			<div class="row-fluid">
				<div class="span12">
					<input type="text" name="dad-fullname" value="<?php echo (isset($data->dad->fullname)) ? $data->dad->fullname : "" ; ?>" placeholder="Nom et prénom du père :">
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<input type="text" name="dad-birthday" value="<?php echo (isset($data->dad->birthday) ? $data->dad->birthday : "") ?>" placeholder="Date de naissance :">
				</div>
				<div class="span6">
					<input type="text" name="dad-profession" value="<?php echo (isset($data->dad->profession) ? $data->dad->profession : "") ?>" placeholder="Profession :">
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<input type="text" name="dad-num_security" value="<?php echo (isset($data->dad->num_security) ? $data->dad->num_security : "") ?>" placeholder="N° Sécurité sociale :">
				</div>
				<div class="span6">
					<input type="text" name="dad-groupe_sanguin" value="<?php echo (isset($data->dad->groupe_sanguin) ? $data->dad->groupe_sanguin : "") ?>" placeholder="Groupe sanguin :">
				</div>
			</div>
			<div class="row-fluid mb10">
				<div class="span12">
					<input type="text" name="dad-maladie_genetique" value="<?php echo (isset($data->dad->fullname) ? $data->dad->fullname : "") ?>" placeholder="Maladie génétique :">
				</div>
			</div>


			<div class="row-fluid">
				<div class="span12">
					<input type="text" name="mom-fullname" value="<?php echo (isset($data->mom->fullname) ? $data->mom->fullname : "") ?>" placeholder="Nom et prénom de la mère :">
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<input type="text" name="mom-birthday" value="<?php echo (isset($data->mom->birthday) ? $data->mom->birthday : "") ?>" placeholder="Date de naissance :">
				</div>
				<div class="span6">
					<input type="text" name="mom-profession" value="<?php echo (isset($data->mom->profession) ? $data->mom->profession : "") ?>" placeholder="Profession :">
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<input type="text" name="mom-num_security" value="<?php echo (isset($data->mom->num_security) ? $data->mom->num_security : "") ?>" placeholder="N° Sécurité sociale :">
				</div>
				<div class="span6">
					<input type="text" name="mom-groupe_sanguin" value="<?php echo (isset($data->mom->groupe_sanguin) ? $data->mom->groupe_sanguin : "") ?>" placeholder="Groupe sanguin :">
				</div>
			</div>
			<div class="row-fluid mb10">
				<div class="span12">
					<input type="text" name="mom-maladie_genetique" value="<?php echo (isset($data->mom->fullname) ? $data->mom->fullname : "") ?>" placeholder="Maladie génétique :">
				</div>
			</div>
	
			
			<ul class="inline button-action">
				<li><input type="submit" name="" value="Enregister" class="btn-link"></li>
				<li><input type="submit" name="" value="Annuler" class="btn-link" onclick="window.parent.SqueezeBox.close();"></li>
			</ul>
		</div>


	</form>
</section>