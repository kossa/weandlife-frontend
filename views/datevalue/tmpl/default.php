<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
$data = $this->data;
$titles = array(
						'tension' => "Tension (mm/HG)", 
						'poids' => "Poids (Kg)", 
						'taille' => "Taille (Cm)",
						'glycemie' => "Glycémie (Gr)",
						'cholesterol' => "Cholestérol (Gr)"
						);
$mesures = array(
						'tension' => "xx.x", 
						'poids' => "xx.x", 
						'taille' => "xx",
						'glycemie' => "x.x",
						'cholesterol' => "x.x"
						);
$patterns = array(
						'tension' => "\d{2}\.\d{1}", 
						'poids' => "\d{1,3}(\.\d{1,2}){0,1}",
						'taille' => "\d{2,3}",
						'glycemie' => "\d\.\d",
						'cholesterol' => "\d\.\d"
						);


$app = JFactory::getApplication();
$model = $app->input->get('model','');
$title = $titles[$model];
$mesure = $mesures[$model];
$pattern = $patterns[$model];
$allvalues = isset($data->$model) ? $data->$model : '';

?>

<section class="p20 date-value">
	<form action="index.php?option=com_weandlife&task=person.setmydatevalue&model=<?php echo $model ?>&tmpl=component" method="POST" class="mon-modal mon-carnet" target="_top">
		<header class="top">
			<h2 class="title left"><?php echo $title ?></h2>
			<div class="access">
				<ul class="inline">
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios1" value="private" <?php if(isset($data->access) && $data->access == "private") echo "checked";?>>
						  Accés privé
						</label>
					</li>
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios2" value="public" <?php if(isset($data->access) && $data->access == "public") echo "checked";?>>
						  Accés public
						</label>
					</li>
				</ul>
			</div>
		</header>
		<div class="the-form">
			<div class="row-fluid">
				<div class="span5">
					<ul class="items content_3 unstyled">
						<li class="today">
							<input type="text" name="datevalue[][<?php echo date("d/m/Y") ?>]" tabindex="1" value="" placeholder="<?php echo $mesure ?>" pattern="<?php echo $pattern ?>">
							<span class="date"><?php echo date("d/m/Y")  ?></span>
						</li>
					<?php 
						if ($allvalues !=''):
							for ($i=0; $i < sizeof($allvalues); $i++) :
								foreach ($allvalues[$i] as $date => $value) :?>
									<li>
										<a href="#" class="delete">X</a>
										<input type="text" name="datevalue[][<?php echo $date ?>]" tabindex="<?php echo $i+2; ?>" value="<?php echo $value ?>" placeholder="<?php echo $mesure ?>" pattern="<?php echo $pattern ?>">
										<span class="date"><?php echo $date ?></span>
									</li>
<?php
								endforeach;
							endfor;
						endif;
?>
					</ul>
				</div>
				<div class="span7">
				<?php
						$zone = "pubs";
						$modules = JModuleHelper::getModules($zone);
						foreach ($modules as $module){
						   echo JModuleHelper::renderModule($module);
						}
				?>
				</div>
			</div>
			<ul class="inline button-action">
				<li><input type="submit" name="" value="Enregister" class="btn-link"></li>
				<li><input type="submit" name="" value="Annuler" class="btn-link" onclick="window.parent.SqueezeBox.close();"></li>
			</ul>
		</div><!-- the-form -->


	</form>
</section>