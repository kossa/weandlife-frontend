<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;

$data = $this->data;
JHTML::_('behavior.calendar');

?>
<form action="index.php?option=com_weandlife&task=person.completeprofile&tmpl=component" method="POST" class="mon-modal mon-carnet" target="_top">
		<header class="top">
			<h3 class="title">Modifier profile</h3>
			<ul class="inline right">
				<li><button type="submit" class="btn btn-link">Valider</button></li>
				<li> / </li>
				<li><a onclick="window.parent.SqueezeBox.close();" href="#" class="red">Annuler</a></li>
			</ul>
		</header>
		<div class="the-form">
			<div class="row-fluid">
				<div class="span8"> <input type="text" name="nom" value="<?php echo (isset($data->nom)) ? $data->nom : '' ?>" placeholder="Nom et Prénom"> </div>
				<div class="span4"> <input type="text" name="date_naissance" value="<?php echo (isset($data->date_naissance) && $data->date_naissance!='0-0-0') ? $data->date_naissance : '' ?>" placeholder="Né(e) JJ/MM/AA"> </div>
			</div>
			<div class="row-fluid">
				<div class="span4"> 
					<select name="gender">
						<option value="Mr">Mr</option>
						<option value="Mme">Mme</option>
						<option value="Mlle">Mlle</option>
					</select>
				</div>
				<div class="span4"> <input type="text" name="nb_enfant" value="<?php echo (isset($data->nb_enfant) && $data->nb_enfant != 0) ? $data->nb_enfant : '' ?>" pattern="\d{1}" placeholder="Nbr enfants <10"> </div>
				<div class="span4"> <input type="email" name="mail" value="<?php echo (isset($data->mail)) ? $data->mail : '' ?>" placeholder="Mail"> </div>
			</div>
			<div class="row-fluid">
				<div class="span6"><input type="password" name="password1" value="" placeholder="Modifier mot de passe" pattern="\w{5,20}"></div>
				<div class="span6"><input type="password" name="password2" value="" placeholder="Confirmer mot de passe" pattern="\w{5,20}"></div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<input type="text" name="fonction" value="<?php echo (isset($data->fonction)) ? $data->fonction : '' ?>" placeholder="Fonction">
				</div>
			</div>
			<div class="row-fluid">
					<div class="span12">
						<input type="text" name="adresse" value="<?php echo (isset($data->adresse)) ? $data->adresse : '' ?>" placeholder="Adresse">
					</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<input type="text" name="ville" value="<?php echo (isset($data->ville)) ? $data->ville : '' ?>" placeholder="Ville">
				</div>
				<div class="span6">
					<select name="wilaya">
						<?php 
							$wilayas = array("Adrar", "Chlef", "Laghouat", "Oum-El-Bouaghi", "Batna", "Bejaia", "Biskra", "Bechar", "Blida", "Bouira", "Tamanrasset", "Tebessa", "Tlemcen", "Tiaret", "Tizi-Ouzou", "Alger", "Djelfa", "Jijel", "Setif", "Saida", "Skikda", "Sidi-Bel-Abbes", "Annaba", "Guelma", "Constantine", "Medea", "Mostaganem", "MSila", "Mascara", "Ouargla", "Oran", "El-Bayadh", "Illizi", "Bordj-Bou-Arreridj", "Boumerdes", "El-Taref", "Tindouf", "Tissemsilt", "El-Oued", "Khenchela", "Souk-Ahras", "Tipaza", "Mila", "Ain-Defla", "Naama", "Ain-Temouchent", "Ghardaia", "Relizane");
							foreach ($wilayas as $wilaya) { ?>
								<option value="<?php echo $wilaya;?>" <?php echo ($data->wilaya == $wilaya) ? "selected" :"" ; ?> ><?php echo $wilaya ?></option>";
						 	<?php
						 } 
						 ?>
						
					</select>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4"> <input type="text" name="mobile1" value="<?php echo (isset($data->mobile1)) ? $data->mobile1 : '' ?>" pattern="^\d" placeholder="Tél. mobile1"></div>
				<div class="span4"> <input type="text" name="mobile2" value="<?php echo (isset($data->mobile2)) ? $data->mobile2 : '' ?>" pattern="\d{10}" placeholder="Tél. mobile2"></div>
				<div class="span4"> <input type="text" name="tel" value="<?php echo (isset($data->tel)) ? $data->tel : '' ?>" pattern="\d{10}" placeholder="Tél. fixe"></div>
				</div>
			</div>
	</div>
</form>