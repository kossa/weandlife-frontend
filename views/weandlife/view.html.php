<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Weandlife.
 */
class WeandlifeViewWeandlife extends JViewLegacy
{

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$result = $this->get("MyHomePage");
		$rh = (isset($result->rh)) ? $result->rh : "" ;
		$tension = ($result->tension != '' && !is_null(json_decode($result->tension))) ? array_values(get_object_vars(json_decode($result->tension)->tension[0])) : "";
		$poids = (!is_null(json_decode($result->poids))) ? array_values(get_object_vars(json_decode($result->poids)->poids[0])) : "";
		$taille = (!is_null(json_decode($result->taille))) ? array_values(get_object_vars(json_decode($result->taille)->taille[0])) : "";
		$glycemie = (!is_null(json_decode($result->glycemie))) ? array_values(get_object_vars(json_decode($result->glycemie)->glycemie[0])) : "";
		$cholesterol = (!is_null(json_decode($result->cholesterol))) ? array_values(get_object_vars(json_decode($result->cholesterol)->cholesterol[0])) : "";
		
		//var_dump($result->taille);
		//var_dump(json_decode($result->taille));
		$this->assignRef('rh',$rh);
		$this->assignRef('tension',$tension[0]);
		$this->assignRef('poids',$poids[0]);
		$this->assignRef('taille',$taille[0]);
		$this->assignRef('glycemie',$glycemie[0]);
		$this->assignRef('cholesterol',$cholesterol[0]);

		//$this->assignRef('taille',$taille[0]);
		//var_dump($poids[0]);
		//$model = JModelLegacy::getInstance('weandlife', 'WeandlifeModel');
		//$this->data = $model->getMyContact();
    parent::display($tpl);
	}
    	
}
