<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
?>

<div class="informations">
	<h1 class="mon-profile"><a href="index.php?option=com_weandlife&amp;view=carnet&amp;tmpl=component&amp;bg=gray" class="modal" rel="{handler: 'iframe', size: {x: 500, y: 320}}">Mon carnet</a></h1>
	<h2><a href="index.php?option=com_weandlife&view=donorgane&tmpl=component" class="modal red box" rel="{handler: 'iframe', size: {x: 500, y: 420}}">Dons d’organes</a></h2>
	<div class="boxes">
	<ul class="inline">
		<li>
			<a href="index.php?option=com_weandlife&view=rh&tmpl=component" data-toggle="modal" class="modal red box" rel="{handler: 'iframe', size: {x: 500, y: 222}}">
				Rhésus (Rh) <br>
				<span class="value"><?php echo $this->rh ?></span>
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datevalue&model=tension&tmpl=component" class="modal green box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">
				Tension (mm Hg) <br>
				<span class="value"><?php echo $this->tension ?></span>
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datevalue&model=cholesterol&tmpl=component" class="modal violet box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Cholestérol (g/l)<br>
				<span class="value"><?php echo $this->cholesterol ?></span>
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datevalue&model=glycemie&tmpl=component" class="modal blue box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Glycémie (Gr)<br>
				<span class="value"><?php echo $this->glycemie ?></span>
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datevalue&model=poids&tmpl=component" class="modal orange box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">
				Poids (kg)<br>
				<span class="value"><?php echo $this->poids ?></span>
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datevalue&model=taille&tmpl=component" class="modal yellow box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Taille (cm)<br>
				<span class="value"><?php echo $this->taille ?></span>
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=consultations&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Mes <br> consultations
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=maladies&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Pathologies <br> (maladies)
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=operations&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Interventions <br>(opérations)
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=traitements&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Traitements médicaments régime 
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=maladies_chroniques&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Maladies génétiques chroniques
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=grossesse&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Grossesse Maternité
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=allergies&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Complications Troubles Allergies
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=accidents&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Accident de travail / civil ou invalidité
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=cure&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Cure, repos et  <br> remise en forme
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=vie_social&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Vie conjugale Communication et vie sociale
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=consommations_interdites&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Consommations interdites
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=datetext&model=assurence&tmpl=component" class="modal gray box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">	
				Assurance
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=vaccins&tmpl=component" class="modal red box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">
				Vaccins
			</a>
		</li>
		<li>
			<a href="index.php?option=com_weandlife&view=rdv&tmpl=component" class="modal red box" rel="{handler: 'iframe', size: {x: 500, y: 330}}">
				Mes <br> Rendz-Vous
			</a>
		</li>
		<li>
		<a href="index.php?option=com_weandlife&view=ficheparent&tmpl=component" class="modal yellow box" rel="{handler: 'iframe', size: {x: 500, y: 440}}">
				Fiche <br> Parents
			</a>
		</li>
		<li>
		<a href="index.php?option=com_weandlife&view=contact&tmpl=component" class="modal green box" rel="{handler: 'iframe', size: {x: 500, y: 370}}">
				Personnes <br> à contacter
			</a>
		</li>
	</ul>
	</div>
</div>
