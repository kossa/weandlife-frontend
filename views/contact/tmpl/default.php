<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
$data = $this->data;

?>

<section class="p20 contact-person">
	<form action="index.php?option=com_weandlife&task=person.setmycontact&tmpl=component" method="POST" class="mon-modal mon-carnet" target="_top">
		<header class="top">
			<h2 class="title left">Personnes à contacter</h2>
			<div class="access">
				<ul class="inline">
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios1" value="private" <?php echo (isset($data->access) && $data->access == "private") ? "checked" : '';?>>
						  Accés privé
						</label>
					</li>
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios2" value="public" <?php echo (isset($data->access) && $data->access == "public") ? "checked" : ''?>>
						  Accés public
						</label>
					</li>
				</ul>
			</div>
		</header>
		<div class="the-form">
			<div class="row-fluid">
				<div class="span12">
					<input type="text" name="fullname1" value="<?php echo (isset($data->person1->fullname)) ? $data->person1->fullname : '' ?>" placeholder="1ère Personne">
				</div>
			</div>
			<div class="row-fluid mb10">
				<div class="span4">
					<input type="text" name="mobile1" value="<?php echo (isset($data->person1->mobile)) ? $data->person1->mobile : '' ?>" placeholder="Tél. mobile">
				</div>
				<div class="span4">
					<input type="text" name="fixe1" value="<?php echo (isset($data->person1->fixe)) ? $data->person1->fixe : '' ?>" placeholder="Tél. fixe">
				</div>
				<div class="span4">
					<input type="email" name="mail1" value="<?php echo (isset($data->person1->mail)) ? $data->person1->mail : '' ?>" placeholder="Mail">
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="span12">
					<input type="text" name="fullname2" value="<?php echo (isset($data->person2->fullname)) ? $data->person2->fullname : '' ?>" placeholder="2ème Personne">
				</div>
			</div>
			<div class="row-fluid mb10">
				<div class="span4">
					<input type="text" name="mobile2" value="<?php echo (isset($data->person2->mobile)) ? $data->person2->mobile : '' ?>" placeholder="Tél. mobile">
				</div>
				<div class="span4">
					<input type="text" name="fixe2" value="<?php echo (isset($data->person2->fixe)) ? $data->person2->fixe : '' ?>" placeholder="Tél. fixe">
				</div>
				<div class="span4">
					<input type="email" name="mail2" value="<?php echo (isset($data->person2->mail)) ? $data->person2->mail : '' ?>" placeholder="Mail">
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="span12">
					<input type="text" name="fullname3" value="<?php echo (isset($data->person3->fullname)) ? $data->person3->fullname : '' ?>" placeholder="3ème Personne">
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4">
					<input type="text" name="mobile3" value="<?php echo (isset($data->person3->mobile)) ? $data->person3->mobile : '' ?>" placeholder="Tél. mobile">
				</div>
				<div class="span4">
					<input type="text" name="fixe3" value="<?php echo (isset($data->person3->fixe)) ? $data->person3->fixe : '' ?>" placeholder="Tél. fixe">
				</div>
				<div class="span4">
					<input type="email" name="mail3" value="<?php echo (isset($data->person3->mail)) ? $data->person3->mail : '' ?>" placeholder="Mail">
				</div>
			</div>
			
			<ul class="inline button-action">
				<li><input type="submit" name="" value="Enregister" class="btn-link"></li>
				<li><input type="submit" name="" value="Annuler" class="btn-link" onclick="window.parent.SqueezeBox.close();"></li>
			</ul>
		</div>


	</form>
</section>