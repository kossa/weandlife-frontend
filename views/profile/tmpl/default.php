<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
$data = $this->data;

$titles = array('consultations' => "Mes Consultations", 
	'maladies' => "Pathologies (maladies)", 
	'operations' => "Interventions (opérations)", 
	'traitements' => "Traitements médicaments ", 
	'maladies_chroniques' => "Maladies génétiques", 
	'grossesse' => "Grossesse Maternité", 
	'allergies' => "Troubles Allergies", 
	'accidents' => "Accidents travail", 
	'cure' => "Cure, repos en forme", 
	'vie_social' => "Communication et vie sociale", 
	'consommations_interdites' => "Consommations interdites", 
	'assurence' => "Assurance");

global $indicators, $xInd;
$indicators = array();
$xInd = 0;

?>


<section class="public-profile">
	<div class="container">
		
		<?php echo $this->loadTemplate('header'); ?>

		<div class="row-fluid">
			<div class="span6 informations">
				<?php echo $this->loadTemplate('boxes'); ?>
			</div>
			<div class="separator"></div>
			<div class="span6">
				<div id="myCarousel0" class="carousel slide">

				  <!-- Carousel items -->
				  <div class="carousel-inner">
				  <?php foreach ($titles as $key => $value):
					  if ($data->$key != '' && strstr($data->$key, '"access":"public"')) : ?>
					  	<div class="item">
					    	<h4 class="title"><?php echo $value; $indicators[$xInd] = $value; $xInd++; ?></h4>
					    	<div class="the-content">
					    		<div class="content_3">
						    		<?php 
						    		$arrayValues = json_decode($data->$key)->$key; // Get The array table

						    		for ($i=0; $i < sizeof($arrayValues); $i++) { 
						    			foreach ($arrayValues[$i] as $theDate => $theValue) :?>
							    			<span class="date"><?php echo $theDate ?></span>
							    			<p><?php echo $theValue ?></p>
							    		<?php endforeach;
						    		} ?>
						    		
					    		</div><!-- content_3 -->
					    	</div><!-- the-content -->
					    </div><!-- item -->
					<?php
					  endif;
				  endforeach ?>
				  <?php 
				  // Add Fiche Parents et Personnes à contact
			  		echo $this->loadTemplate('fiche_contact');
				   ?>
				  </div><!-- carousel-inner -->
				  <!-- Carousel nav -->
				  <a class="carousel-control left" href="#myCarousel0" data-slide="prev">&lsaquo;</a>
				  <a class="carousel-control right" href="#myCarousel0" data-slide="next">&rsaquo;</a>
				</div>
			</div>
		</div>

		<div class="indicators">
			<ol class="carousel-indicators inline">
			<?php foreach ($indicators as $key => $value): ?>
				<li data-target="#myCarousel0" data-slide-to="<?php echo $key ?>"><?php echo $value ?> </li>
			<?php endforeach ?>
		  </ol>
		</div>
	</div>

</section>