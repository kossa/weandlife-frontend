<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
$data = $this->data;
$fiche_parent = json_decode($data->fiche_parent);
$to_contact = json_decode($data->to_contact);
global $indicators, $xInd;
?>

<?php if (!is_null($fiche_parent) && $fiche_parent->access == 'public'): ?>
	<div class="item">
		<h4 class="title">Fiche Parents</h4>
		<div class="the-content fiche-parents">
			<div class="content_3">
				<p>Nom et prénom du père: <span class="gray"><?php echo $fiche_parent->dad->fullname; ?></span></p>
				<p>Date de naissance du père: <span class="gray"><?php echo $fiche_parent->dad->birthday; ?></span></p>
				<p>Profession du père: <span class="gray"><?php echo $fiche_parent->dad->profession; ?></span></p>
				<p>N° Sécurité sociale: <span class="gray"><?php echo $fiche_parent->dad->num_security; ?></span></p>
				<p>Groupe sanguin: <span class="gray"><?php echo $fiche_parent->dad->groupe_sanguin; ?></span></p>
				<p>Maladie génétique: <span class="gray"><?php echo $fiche_parent->dad->maladie_genetique; ?></span></p>
	  		<br>
	  		<p>Nom et prénom du mère: <span class="gray"><?php echo $fiche_parent->mom->fullname; ?></span></p>
				<p>Date de naissance du mère: <span class="gray"><?php echo $fiche_parent->mom->birthday; ?></span></p>
				<p>Profession du mère: <span class="gray"><?php echo $fiche_parent->mom->profession; ?></span></p>
				<p>N° Sécurité sociale: <span class="gray"><?php echo $fiche_parent->mom->num_security; ?></span></p>
				<p>Groupe sanguin: <span class="gray"><?php echo $fiche_parent->mom->groupe_sanguin; ?></span></p>
				<p>Maladie génétique: <span class="gray"><?php echo $fiche_parent->mom->maladie_genetique; ?></span></p>
			</div><!-- content_3 -->
		</div><!-- the-content -->
	</div><!-- item -->
<?php 
		$indicators[$xInd] = "Fiche Parents"; $xInd++;
		endif ?>


<?php if (!is_null($to_contact) && $to_contact->access == 'public'): ?>
	<div class="item">
		<h4 class="title">Personnes à contacter</h4>
		<div class="the-content">
			<div class="content_3 to-contact">
				<h3>Person1</h3>
				<p>Nom et prénom: <span class="gray"><?php echo $to_contact->person1->fullname; ?></span></p>
				<p>mobile: <span class="gray"><?php echo $to_contact->person1->mobile; ?></span></p>
				<p>fixe: <span class="gray"><?php echo $to_contact->person1->fixe; ?></span></p>
				<p>mail: <span class="gray"><?php echo $to_contact->person1->mail; ?></span></p>

				<h3>Person2</h3>
				<p>Nom et prénom: <span class="gray"><?php echo $to_contact->person2->fullname; ?></span></p>
				<p>mobile: <span class="gray"><?php echo $to_contact->person2->mobile; ?></span></p>
				<p>fixe: <span class="gray"><?php echo $to_contact->person2->fixe; ?></span></p>
				<p>mail: <span class="gray"><?php echo $to_contact->person2->mail; ?></span></p>

				<h3>Person3</h3>
				<p>Nom et prénom: <span class="gray"><?php echo $to_contact->person3->fullname; ?></span></p>
				<p>mobile: <span class="gray"><?php echo $to_contact->person3->mobile; ?></span></p>
				<p>fixe: <span class="gray"><?php echo $to_contact->person3->fixe; ?></span></p>
				<p>mail: <span class="gray"><?php echo $to_contact->person3->mail; ?></span></p>

			</div><!-- content_3 -->
		</div><!-- the-content -->
	</div><!-- item -->
<?php 
		$indicators[$xInd] = "Personnes à contacter"; $xInd++;
		endif ?>
