<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;

$data = $this->data;
$name = $data->nom;
$age = WeandlifeHelper::getAge($data->date_naissance);
$nb_enfant = $data->nb_enfant;
$fonction = $data->fonction;
$mail = $data->mail;
$mobile1 = $data->mobile1;
$mobile2 = $data->mobile2;
$tel = $data->tel;
$adresse = $data->adresse;
$ville = $data->ville;
$donorgane = WeandlifeHelper::getDonneurs($data->donorgane);
?>

<header>
	<h3 class="name"><?php echo $name ?></h3>
	<p class="information">
		<?php if ($age != 0): ?>
			<span><?php echo $age ?> ans</span>
		<?php endif ?>
		<span><?php echo $nb_enfant ?> enfant(s)</span>
		<span><?php echo $fonction ?></span>
		<span><?php echo $mail ?></span>
		<?php echo ($mobile1 != "") ? "<span>$mobile1</span>" : "" ?>
		<?php echo ($mobile2 != "") ? "<span>$mobile2</span>" : "" ?>
		<?php echo ($tel != "") ? "<span>$tel</span>" : "" ?>
			
		<?php echo ($adresse != '' || $ville !='') ? '<span>'.$adresse. ' - ' .$ville.'</span>': '';  ?>
	</p>
	<p class="donneurs">
		Don : <?php echo $donorgane ?>
	</p>
</header>