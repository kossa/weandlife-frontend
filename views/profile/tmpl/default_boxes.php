<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;

$data = $this->data;
$tension = ($data->tension != '') ? WeandlifeHelper::getLatestValue($data->tension, 'tension'): '';
$poids = ($data->poids != '') ? WeandlifeHelper::getLatestValue($data->poids, 'poids'): '';
$taille = ($data->taille != '') ? WeandlifeHelper::getLatestValue($data->taille, 'taille'): '';
$glycemie = ($data->glycemie != '') ? WeandlifeHelper::getLatestValue($data->glycemie, 'glycemie'): '';
$cholesterol = ($data->cholesterol != '') ? WeandlifeHelper::getLatestValue($data->cholesterol, 'cholesterol'): '';
?>

<div class="boxes">
	<ul class="inline">
		<li><a class="red box">Rhésus (Rh)<br><span class="value"><?php echo $data->rh ?></span></a></li>
		<li><a class="green box">Tension (mm Hg)<br><span class="value"><?php echo $tension ?></span></a></li>
		<li><a class="violet box">Cholestérol (g/l)<br><span class="value"><?php echo $cholesterol ?></span></a></li>
		<li><a class="blue box">Glycémie (gr)<br><span class="value"><?php echo $glycemie ?></span></a></li>
		<li><a class="orange box">Poids (kg)<br><span class="value"><?php echo $poids ?></span></a></li>
		<li><a class="yellow box">Taille (cm)<br><span class="value"><?php echo $taille ?></span></a></li>
	</ul>
</div>