<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
$data = $this->data;
$titles = array(
						'consultations' => "Mes Consultations", 
						'maladies' => "Pathologies (maladies)", 
						'operations' => "Interventions (opérations)",
						'traitements' => "Traitements médicaments ",
						'maladies_chroniques' => "Maladies génétiques",
						'grossesse' => "Grossesse Maternité",
						'allergies' => "Troubles Allergies",
						'accidents' => "Accidents travail",
						'cure' => "Cure, repos en forme",
						'vie_social' => "Communication et vie sociale",
						'consommations_interdites' => "Consommations interdites",
						'assurence' => "Assurance"
						);

$app = JFactory::getApplication();
$model = $app->input->get('model','');
$title = $titles[$model];
$allvalues = isset($data->$model) ? $data->$model : '';

?>

<section class="p20 date-value date-text">
	<form action="index.php?option=com_weandlife&task=person.setmydatevalue&model=<?php echo $model ?>&tmpl=component" method="POST" class="mon-modal mon-carnet" target="_top">
		<header class="top">
			<h2 class="title left"><?php echo $title ?></h2>
			<div class="access">
				<ul class="inline">
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios1" value="private" <?php if(isset($data->access) && $data->access == "private") echo "checked";?>>
						  Accés privé
						</label>
					</li>
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios2" value="public" <?php if(isset($data->access) && $data->access == "public") echo "checked";?>>
						  Accés public
						</label>
					</li>
				</ul>
			</div>
		</header>
		<div class="the-form">
			<div class="row-fluid">
				<div class="span12">
					<ul class="items content_3 unstyled">
						<li class="today">
							<span class="date"><?php echo date("d/m/Y")  ?></span>
							<textarea name="datevalue[][<?php echo date("d/m/Y") ?>]" tabindex="1"></textarea>
						</li>
					<?php 
					if ($allvalues !=''):
						for ($i=0; $i < sizeof($allvalues); $i++) :
							foreach ($allvalues[$i] as $date => $value) :?>
								<li>
									<span class="date"><?php echo $date ?></span>
									<a href="#" class="delete">X</a>
									<textarea name="datevalue[][<?php echo $date ?>]" tabindex="<?php echo $i+2; ?>" rows="<?php echo ceil(strlen($value) / 58) ?>"><?php echo $value ?></textarea>
								</li>
<?php
							endforeach;
						endfor;
					endif;
?>
					</ul>
				</div>
			</div>
			<ul class="inline button-action">
				<li><input type="submit" name="" value="Enregister" class="btn-link"></li>
				<li><input type="submit" name="" value="Annuler" class="btn-link" onclick="window.parent.SqueezeBox.close();"></li>
			</ul>
		</div><!-- the-form -->


	</form>
</section>