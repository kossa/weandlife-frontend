<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
//var_dump($this->data);
$data = $this->data;

?>

<section class="p20 don-organe">
	<form action="index.php?option=com_weandlife&task=person.setmydonorgane&tmpl=component" method="POST" class="mon-modal mon-carnet" target="_top">
		<header class="top">
			<h2 class="title left">Dons d'organes</h2>
			<div class="access">
				<ul class="inline">
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios1" value="private" <?php if(isset($data->access) && $data->access == "private") echo "checked";?>>
						  Accés privé
						</label>
					</li>
					<li>
						<label class="radio">
						  <input type="radio" name="access" id="optionsRadios2" value="public" <?php if(isset($data->access) && $data->access == "public") echo "checked";?>>
						  Accés public
						</label>
					</li>
				</ul>
			</div>
		</header>
		<div class="the-form">
			
			<div class="row-fluid">
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="sang" value="checked" <?php echo isset($data->sang) ? $data->sang : ''; ?>><span>Sang</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="plaquettes" value="checked" <?php echo isset($data->plaquettes) ? $data->plaquettes : ''; ?>><span>Plaquettes</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="foie" value="checked" <?php echo isset($data->foie) ? $data->foie : ''; ?>><span>Foie</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="rein" value="checked" <?php echo isset($data->rein) ? $data->rein : ''; ?>><span>Rein</span></label>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="coeur" value="checked" <?php echo isset($data->coeur) ? $data->coeur : ''; ?>><span>Cœur</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="lobe" value="checked" <?php echo isset($data->lobe) ? $data->lobe : ''; ?>><span>Lobe hépatique / pulmonaire</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="poumon" value="checked" <?php echo isset($data->poumon) ? $data->poumon : ''; ?>><span>Poumon</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="pancreas" value="checked" <?php echo isset($data->pancreas) ? $data->pancreas : ''; ?>><span>Pancrèas</span></label>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="coeur_poumon" value="checked" <?php echo isset($data->coeur_poumon) ? $data->coeur_poumon : ''; ?>><span>Coeur-poumon</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="intestin" value="checked" <?php echo isset($data->intestin) ? $data->intestin : ''; ?>><span>Intestin</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="cornee" value="checked" <?php echo isset($data->cornee) ? $data->cornee : ''; ?>><span>Cornée</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="os" value="checked" <?php echo isset($data->os) ? $data->os : ''; ?>><span>Os</span></label>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="cartilage" value="checked" <?php echo isset($data->cartilage) ? $data->cartilage : ''; ?>><span>Cartilage</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="fragments" value="checked" <?php echo isset($data->fragments) ? $data->fragments : ''; ?>><span>Fragments osseux</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="moelle" value="checked" <?php echo isset($data->moelle) ? $data->moelle : ''; ?>><span>Mœlle osseuse</span></label>
				</div>
				<div class="span3">
					<label class="checkbox"><input type="checkbox" name="tissus" value="checked" <?php echo isset($data->tissus) ? $data->tissus : ''; ?>><span>Tissus épidermique</span></label>
				</div>
			</div>
		</div>
			
			<ul class="inline button-action">
				<li><input type="submit" name="" value="Enregister" class="btn-link"></li>
				<li><input type="submit" name="" value="Annuler" class="btn-link" onclick="window.parent.SqueezeBox.close();"></li>
			</ul>
		</div>


	</form>
</section>