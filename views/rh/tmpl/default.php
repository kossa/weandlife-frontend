<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
?>

<div class="p10-20">
	<h2 class="title">Rhésus</h2>
	<section class="rhesus">
		<ul class="inline">
			<li><a target="_top" href="index.php?option=com_weandlife&task=person.setrh&tmpl=component&rh=<?php echo urlencode('A+') ?>">A+</a></li>
			<li><a target="_top" href="index.php?option=com_weandlife&task=person.setrh&tmpl=component&rh=<?php echo urlencode('B+') ?>">B+</a></li>
			<li><a target="_top" href="index.php?option=com_weandlife&task=person.setrh&tmpl=component&rh=<?php echo urlencode('AB+') ?>">AB+</a></li>
			<li><a target="_top" href="index.php?option=com_weandlife&task=person.setrh&tmpl=component&rh=<?php echo urlencode('O+') ?>">O+</a></li>
		</ul>
		<ul class="inline">
			<li><a target="_top" href="index.php?option=com_weandlife&task=person.setrh&tmpl=component&rh=<?php echo urlencode('A-') ?>">A-</a></li>
			<li><a target="_top" href="index.php?option=com_weandlife&task=person.setrh&tmpl=component&rh=<?php echo urlencode('B-') ?>">B-</a></li>
			<li><a target="_top" href="index.php?option=com_weandlife&task=person.setrh&tmpl=component&rh=<?php echo urlencode('AB-') ?>">AB-</a></li>
			<li><a target="_top" href="index.php?option=com_weandlife&task=person.setrh&tmpl=component&rh=<?php echo urlencode('O-') ?>">O-</a></li>
		</ul>
	
	</section>
</div>