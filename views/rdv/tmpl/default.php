<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
// no direct access
defined('_JEXEC') or die;
$data = $this->data;
$rdvs = isset($data->rdv) ? $data->rdv :'';
JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
?>

<section class="p20 date-value rdvs">
	<form action="index.php?option=com_weandlife&task=person.setMyRDV&tmpl=component" method="POST" class="mon-modal mon-carnet" target="_top">
		<header class="top">
			<h2 class="title left">Mes Rendez-Vous</h2>
			
		</header>
		<div class="the-form">
			<div class="row-fluid">
				<div class="span12">
					<ul class="items content_3 unstyled">
						<li class="today">
							<input type="text" name="rdv[]" tabindex="1" value="" placeholder="Ajouter un RDV" class="rdv-txt">
							<input type="text" name="date[]" tabindex="1" value="" placeholder="Ajouter une DATE" class="rdv-date">
						</li>
<?php 
						if ($rdvs !=''):
							for ($i=0; $i < sizeof($rdvs); $i++) :
								foreach ($rdvs[$i] as $rdv => $date) :
									$class = (strtotime(date("Y-m-d"))>strtotime($date)) ? "old": "";
									?>
									<li class="<?php echo $class ?>">
										<a href="#" class="delete">X</a>
										<input type="text" name="rdv[]" tabindex="<?php echo $i+2; ?>" value="<?php echo $rdv ?>" placeholder="Ajouter un RDV" class="rdv-txt">
										<input type="text" name="date[]" tabindex="<?php echo $i+2; ?>" value="<?php echo WeandlifeHelper::decryptdate($date) ?>" placeholder="Ajouter une DATE" class="rdv-date">
									</li>
<?php
								endforeach;
							endfor;
						endif;
?>
					</ul>
				</div>
			</div>

			<ul class="inline button-action">
				<li><input type="submit" name="" value="Enregister" class="btn-link"></li>
				<li><input type="submit" name="" value="Annuler" class="btn-link" onclick="window.parent.SqueezeBox.close();"></li>
			</ul>
		</div><!-- the-form -->


	</form>
</section>