<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
 
// No direct access
defined('_JEXEC') or die;

class WeandlifeModelWeandlife extends JModelLegacy {

	/**
	 Get Home page
	*/
	public function getMyHomePage()	{
		$id = JFactory::getUser()->id;
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$query
		    ->select($db->quoteName(array('rh', 'tension', 'poids', 'taille','glycemie','cholesterol')))
		    ->from($db->quoteName('#__weandlife_person'))
		    ->where($db->quoteName('id_user') . ' = '. $id);
		
		$db->setQuery($query);
		return $db->loadObject();
	}

	/**
	 Get one profile for home page of Component
	*/
	Public function getMyprofile() {

		$id = JFactory::getUser()->id;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query
		    ->select(array('id_user', 'id_user', 'nom', 'mail', 'date_naissance', 'gender', 'nb_enfant', 'fonction', 'adresse', 'ville', 'wilaya', 'mobile1', 'mobile2', 'tel'))
		    ->from('#__weandlife_person')
		    ->where('id_user='.$id);
		    
    $db->setQuery($query);
    $results = $db->loadObject();
	
		JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
    $results->date_naissance = WeandlifeHelper::decryptdate($results->date_naissance);
		return $results;
	}

	/**
	 Set the password 
	*/
	 public function setPassword($pass='') {
	 	$db = JFactory::getDBO();
	 	$query = $db->getQuery(TRUE);
	 	$object = new stdClass();
	 	$object->id = JFactory::getUser()->id;
	 	$object->password = $pass;
	 	return JFactory::getDbo()->updateObject('#__users', $object,'id', 'false' );
	 	
	 }


	/**
	 Get one profile for home page of Component
	*/
	Public function getProfile($id) {

		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query
		    ->select(array('*'))
		    ->from('#__weandlife_person')
		    ->where('id='.$id);
		    
    $db->setQuery($query);
    $results = $db->loadObject();
	
		JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
    $results->date_naissance = WeandlifeHelper::decryptdate($results->date_naissance);
		return $results;
	}

	/**
	 Set Profile
	*/
	public function setProfile($object) {
		return JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');
	}

	/**
	 Set Rhesus
	*/
	public function setRh($id, $rh) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$object = new stdClass();
		$object->id_user = $id;
		$object->rh = $rh;
		return JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');
	}


	/**
	 Get contact form
	*/
	public function getMyContact() {
		$id = JFactory::getUser()->id;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query
		    ->select(array('to_contact'))
		    ->from('#__weandlife_person')
		    ->where('id_user='.$id);
		    
    $db->setQuery($query);
    return json_decode($db->loadObject()->to_contact);
	}

	/**
	 Set Contact
	*/
	public function setContact($id, $str) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$object = new stdClass();
		$object->id_user = $id;
		$object->to_contact = $str;
		return JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');
	}

	/**
	 Get Parent Form
	*/
	public function getMyFicheParent() {
		$id = JFactory::getUser()->id;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query
		    ->select(array('fiche_parent'))
		    ->from('#__weandlife_person')
		    ->where('id_user='.$id);
		    
    $db->setQuery($query);
    return json_decode($db->loadObject()->fiche_parent);
	}

	/**
	 Set Parent Form
	*/
	public function setFicheParent($id, $str) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$object = new stdClass();
		$object->id_user = $id;
		$object->fiche_parent = $str;
		return JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');

	}

	/** 
	 Get Date value
	*/
	public function getMyDateValue($model) {
		$id = JFactory::getUser()->id;

		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query
		    ->select(array($model))
		    ->from('#__weandlife_person')
		    ->where('id_user='.$id);
		    
    $db->setQuery($query);
    return( json_decode($db->loadObject()->$model) );
	}

	/** 
	 Set DateValue
	*/
	public function setDateValue($model, $str, $id) {		
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$object = new stdClass();
		$object->id_user = $id;
		$object->$model = $str;
		return JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');
	}

	/**
	 Get my Don Organe
	*/
	public function getMyDonOrgane()	{
		$id = JFactory::getUser()->id;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query
		    ->select(array('donorgane'))
		    ->from('#__weandlife_person')
		    ->where('id_user='.$id);
		    
    $db->setQuery($query);
    return json_decode($db->loadObject()->donorgane);
  }

  /**
	 Set my Don Organe
	*/
	 public function setDonOrgane($str, $id) {
  	$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$object = new stdClass();
		$object->id_user = $id;
		$object->donorgane = $str;
		return JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');
  }

	/**
	 Get My RDV
	*/
	public function getMyRDV()	{
		$id = JFactory::getUser()->id;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query
		    ->select(array('rdv'))
		    ->from('#__weandlife_person')
		    ->where('id_user='.$id);
		    
    $db->setQuery($query);
    return json_decode($db->loadObject()->rdv);
  }

	/**
   Set My RDV list
  */
  public function setRDV($str, $id) {
  	$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$object = new stdClass();
		$object->id_user = $id;
		$object->rdv = $str;
		return JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');
  }

	/**
	 Get My vaccins
	*/
	public function getMyvaccins()	{
		$id = JFactory::getUser()->id;
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$query
		    ->select($db->quoteName(array('date_naissance', 'vaccins')))
		    ->from('#__weandlife_person')
		    ->where('id_user='.$id);
		    
    $db->setQuery($query);
    $result = $db->loadObject();
    if($result->vaccins == ''){// Je pense que ce n'est pas necessaire :p
			JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
			$vaccins = WeandlifeHelper::VaccinsBluid($result->date_naissance);
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE);
			$object = new stdClass();
			$object->id_user = $id;
			$object->vaccins = $vaccins;
			JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');
			$result->vaccins = $vaccins;
    }

  	return $result;
  }
	/**
   Set My Vaccins list
  */
  public function setmyVaccins($str, $id) {
  	$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$object = new stdClass();
		$object->id_user = $id;
		$object->vaccins = $str;
		return JFactory::getDbo()->updateObject('#__weandlife_person', $object,'id_user','false');
  }

  /**
  Get RDV Tomorrow
  */
  public function getRdvTomorrow($date)
  {
  	$db = JFactory::getDBO();
  	$query = $db->getQuery(TRUE);
  	$query
  	    ->select($db->quoteName(array('mail','rdv','')))
  	    ->from($db->quoteName('#__user_profiles'))
  	    ->where($db->quoteName('profile_key') . ' LIKE '. $db->quote('\'custom.%\''))
  	    ->order('ordering ASC');
  	
  	$db->setQuery($query);
  	$results = $db->loadObjectList();
  }


  /**
  Get Result from request
  */
	public function getSearch($q, $donorgane, $wilaya)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		//$db->quote( '"'.$donorgane . '": "checked"');

		$query
		    ->select($db->quoteName(array('P.id','nom')))
		    ->from(array($db->quoteName('#__weandlife_person','P'), $db->quoteName('#__users','U')))
		    ->where($db->quoteName('P.id_user') .' = '.$db->quoteName('U.id') .' AND '.$db->quoteName('U.block').' = 0');

		$query->where('('.$db->quoteName('donorgane') . ' LIKE '. $db->quote('%"access":"public"%') . " OR " . $db->quoteName('donorgane') .'="")' ); // Only Public Giver

		if ($donorgane != '') {
			$query->where($db->quoteName('donorgane') . ' LIKE '. $db->quote('%"'. $donorgane .'":"checked"%')); 
		}
		if ($wilaya != '') {
			$query->where($db->quoteName('wilaya') . ' = '. $db->quote($wilaya));
		}
		if ($q != '') {
			$query->where($db->quoteName('nom') . ' LIKE '. $db->quote('%'. $q .'%'));
		}

		$db->setQuery($query);
		$results = $db->loadObjectList();
		return json_encode($results);
	}
}
