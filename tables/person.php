<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
 
// No direct access
defined('_JEXEC') or die;

class TablePerson extends JTable
{

	var $id = null;
	var $id_user = null;
	var $nom = null;
	var $mail = null;
	var $date_naissance = null;
	var $gender = null;
	var $nb_enfant = 0;
	var $fonction = null;
	var $adresse = null;
	var $ville = null;
	var $wilaya = null;
	var $mobile1 = null;
	var $mobile2 = null;
	var $tel = null;
	var $rh = null;
	var $tension = null;
	var $taille = null;
	var $vaccins = null;
	var $consultations = null;
	var $maladies = null;
	var $operations = null;
	var $traitements = null;
	var $maladies_chroniques = null;
	var $grossesse = null;
	var $allergies = null;
	var $physiques = null;
	var $accidents = null;
	var $cure = null;
	var $vie_social = null;
	var $consommations_interdites = null;
	var $assurence = null;
	var $rdv = null;
	var $fiche_parent = null;
	var $to_contact = null;
	var $date_inscription = null;

	public function __construct( &$db ) {
	    parent::__construct('#__weandlife_person', 'id', $db);
  }
}