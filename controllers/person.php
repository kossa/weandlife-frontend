<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class WeandlifeControllerPerson extends JControllerLegacy
{

	/**
	 * Logout function
	 */
	public function setDeconnexion() {
	    $app = JFactory::getApplication();
	    $app->logout();
	    $this->setRedirect("index.php");
	}

	/**
	 Set profile information
	*/
	public function completeprofile()	{
		self::formsent();
		$app = JFactory::getApplication();
		$jinput = $app->input;
		$object = new stdClass();

		try {
			JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
			$object = WeandlifeHelper::GetCompleteProfileObject($jinput);

			// Set The profile information
			$result = $this->getModel("Weandlife")->setProfile($object);

			// Set Password
			$pass1 = $jinput->get('password1');
			$pass2 = $jinput->get('password2');
			$str='';
			if ($pass1 == $pass2 && $pass1 != '') {
				$result = $this->getModel("Weandlife")->setPassword(md5($pass1));
				$str=', Et le mot de passe a été modifié' ;
			}else if($pass1 != $pass2){
				$str = ', Les 2 mots de passe sont différents';
			}

			$app->redirect("index.php?option=com_weandlife", "Informations prises en charge".$str,'success');

			
		} catch (Exception $e) {
			$app->redirect("index.php?option=com_weandlife", "Une erreur a été produite: ".$e,"error");
		}
	}

	/**
	 Set Rhesus
	*/
	public function setRh()	{
		$app = JFactory::getApplication();
		$id = JFactory::getUser()->id;
		if ($id>0) {
			$rh = $app->input->get('rh','','STRING');
			// Call Model
			$result = $this->getModel('Weandlife')->setRh($id, $rh);
			$app->redirect("index.php?option=com_weandlife", "Votre Rhésus a été modifié ","success");
		}else{
			$app->redirect("index.php?option=com_users&view=login", "Veuillez vous connecter","info");
		}
	}

	/**
	 Set my contacts 
	*/
	public function setMyContact() {
		self::formsent();
		$app = JFactory::getApplication();
		$id = JFactory::getUser()->id;
		if ($id>0) {
			$jinput = $app->input;
			JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
			$str =  WeandlifeHelper::getContactString($jinput);

			if ($this->getModel('Weandlife')->setContact($id, $str)){
				$app->redirect("index.php?option=com_weandlife", "Vos contacts ont été modifié ","success");
			}else{
				$app->redirect("index.php?option=com_weandlife", "Problème lors de la sauvegarde :( ",'error');
			}	
		}else{
			$app->redirect("index.php?option=com_users&view=login", "Veuillez vous connecter","info");
		}
	}

	/**
	 Set my contacts 
	*/
	public function setMyFicheParent() {
		self::formsent();
		$app = JFactory::getApplication();
		$id = JFactory::getUser()->id;
		if ($id>0) {
			$jinput = $app->input;
			JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
			$str =  WeandlifeHelper::getFicheParentString($jinput);

			if ($this->getModel('Weandlife')->setFicheParent($id, $str)){
				$app->redirect("index.php?option=com_weandlife", "La fiche parent a été modifiée ","success");
			}else{
				$app->redirect("index.php?option=com_weandlife", "Problème lors de la sauvegarde :( ",'error');
			}	
		}else{
			$app->redirect("index.php?option=com_users&view=login", "Veuillez vous connecter","info");
		}
	}

	/**
	 Set my date Values
	*/
	public function setmydatevalue() {
		self::formsent();
		$app = JFactory::getApplication();
		$id = JFactory::getUser()->id;
		if ($id>0) {
			$jinput = $app->input;
			$model = $jinput->get('model',''); // Get the model
			$datevalue = $jinput->get('datevalue','','ARRAY'); // Get the form

			// Create de String
			JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
			$str = WeandlifeHelper::getMyDateValueString($jinput,$model, $datevalue);

			if ($this->getModel('Weandlife')->setDateValue($model,  $str, $id)){
				$msg = WeandlifeHelper::getMsg($model);
				$app->redirect("index.php?option=com_weandlife", $msg." : Mise à jour réussie","success");
			}else{
				$app->redirect("index.php?option=com_weandlife", "Problème lors de la sauvegarde :( ",'error');
			}	
		}else{
			$app->redirect("index.php?option=com_users&view=login", "Veuillez vous connecter","info");
		}
	}

	/**
	 Set my don Organe
	*/
	public function setmydonorgane() {
		self::formsent();
		$app = JFactory::getApplication();
		$id = JFactory::getUser()->id;
		if ($id>0) {
			$jinput = $app->input;
			JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
			$str = WeandlifeHelper::getMyDonOrganeString($jinput);
			if ($this->getModel('Weandlife')->setDonOrgane($str, $id)){
				$app->redirect("index.php?option=com_weandlife", "Votre liste de Dons a été modifiée ","success");
			}
		}else{
			$app->redirect("index.php?option=com_users&view=login", "Veuillez vous connecter","info");
		}

	}

	/**
		Set My RDV
	*/
	public function setMyRDV() {
		self::formsent();
		$app = JFactory::getApplication();
		$id = JFactory::getUser()->id;
		if ($id>0) {
			$jinput = $app->input;
			JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
			$str = WeandlifeHelper::getMyRdvString($jinput);
			if ($this->getModel('Weandlife')->setRDV($str, $id)){
				$app->redirect("index.php?option=com_weandlife", "Votre liste de Rendez-vous a été modifiée ","success");
			}
		}else{
			$app->redirect("index.php?option=com_users&view=login", "Veuillez vous connecter","info");
		}

	}

	/**
		Set My Vaccins
	*/
	public function setMyVaccins() {
		self::formsent();

		$app = JFactory::getApplication();
		$id = JFactory::getUser()->id;
		if ($id>0) {
			$jinput = $app->input;
			JLoader::register('WeandlifeHelper', JPATH_COMPONENT.'/helpers/weandlife.php');
			$str = WeandlifeHelper::getMyRdvString($jinput);
			if ($this->getModel('Weandlife')->setmyvaccins($str, $id)){
				$app->redirect("index.php?option=com_weandlife", "Votre liste de Vaccins a été modifiée ","success");
			}
		}else{
			$app->redirect("index.php?option=com_users&view=login", "Veuillez vous connecter","info");
		}
	}

	/**
		Get RDV tomorrow
	*/
	public function getRdvTomorrow() {
		$result = $this->getModel('Weandlife')->getRdvTomorrow($str, $id);

		var_dump($result);

		exit();
	}

	/**
		Private From sent
	*/
	private static function formsent(){
		if (empty($_POST)){
			$app = JFactory::getApplication();
			$app->redirect("index.php?option=com_weandlife", "s'il vous plaît utilisez le formulaire ;)","error");
		}
	}

}