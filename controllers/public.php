<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class WeandlifeControllerPublic extends JControllerLegacy
{	
		/**
		Get Search
		*/
		public function getSearh()
		{
			$app = JFactory::getApplication();
			$jinput = $app->input;
		 	$q = $jinput->get('q','','STRING');	 	
		 	$donorgane = $jinput->get('donorgane','','STRING');	 	
		 	$wilaya = $jinput->get('wilaya','','STRING');	 	
		 	//echo "$q $donorgane $wilaya";
		 	
			$result = $this->getModel("Weandlife")->getSearch($q, $donorgane, $wilaya);
			echo ($result == '[]') ? ' ': $result;
			exit();
			//echo ($result == '[]') ? ' ': '<h1>'.$result.'</h1>';
		}

		/**
		getProfile
		*/
		public function getProfile()
		{
			$app = JFactory::getApplication();
			$id = $app->input->get('id', 0,'INT');
			if ($id == 0) {
				$app->redirect(".php?option=com_content&view=article&id=4&Itemid=120");
			}
			$result = $this->getModel('Weandlife')->getProfile($id);
			if(is_null($result->id_user)){
				$app->redirect(".php?option=com_content&view=article&id=4&Itemid=120");
			}
			
			$view   = $this->getView('profile', 'html'); //get the view
			$view->assignRef('data', $result); // assign data from the model
			$view->display(); // display the view
		}

		/**
		getProfile
		*/
		public function cronNofication()
		{
		 	$headers .= 'To: hadjikouceyla@gmail.com' . "\r\n";
     	$headers .= 'From: '.'hadjikouceyla@gmail.com'. "\r\n";
			$to = "hadjikouceyla@gmail.com";
			$subject = "Cron from Joomla";
			mail($to,$subject,"Cron from Joomla", $headers);
			echo "ok";
		}
}