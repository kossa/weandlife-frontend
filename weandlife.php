<?php
/**
 * @version     1.0.0
 * @package     com_weandlife
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Kouceyla Hadji <hadjikouceyla@gmail.com> - http://www.behance.net/kossa
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

// Include all tables
JTable::addIncludePath(JPATH_COMPONENT.'/tables');

//load classes
JLoader::registerPrefix('Weandlife', JPATH_COMPONENT);

// Execute the task.
$controller	= JControllerLegacy::getInstance('Weandlife');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
